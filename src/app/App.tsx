import {Suspense} from 'react'
import {Outlet} from 'react-router-dom'
import Notify from '../global/Notify'
import {I18nProvider} from '../_metronic/i18n/i18nProvider'
import {LayoutProvider, LayoutSplashScreen} from '../_metronic/layout/core'
import {MasterInit} from '../_metronic/layout/MasterInit'
import {AuthInit} from './modules/auth'

const App = () => {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <I18nProvider>
        <LayoutProvider>
        <Notify>
          <AuthInit>
            <Outlet />
            <MasterInit />
          </AuthInit>
          </Notify>
        </LayoutProvider>
      </I18nProvider>
    </Suspense>
  )
}

export {App}
