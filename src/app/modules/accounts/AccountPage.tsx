import React, {useEffect} from "react"
import {Navigate, Route, Routes, Outlet} from "react-router-dom"
import {PageLink, PageTitle} from "../../../_metronic/layout/core"
import {Overview} from "./components/Overview"
import {Settings} from "./components/settings/Settings"
import {AccountHeader} from "./AccountHeader"
import {actionFetchUserProfile} from "../../../redux/auth/auth.actions"
import {connect} from "react-redux"
import {createStructuredSelector} from "reselect"
import {selectUserProfileInfo} from "../../../redux/auth/auth.selectors"
const accountBreadCrumbs: Array<PageLink> = [
  {
    title: "Account",
    path: "/account",
    isSeparator: false,
    isActive: false,
  },
  {
    title: "",
    path: "",
    isSeparator: true,
    isActive: false,
  },
]
type InventoriesProps = {
  profile: any
}

const AccountPage: React.FC<InventoriesProps> = ({profile}) => {
  return (
    <Routes>
      <Route
        path='/overview'
        element={
          <>
            <PageTitle breadcrumbs={accountBreadCrumbs}>Overview</PageTitle>
            <AccountHeader actionFetchUserProfile={actionFetchUserProfile} profile={profile} />
          </>
        }
      />
      <Route
        path='/settings'
        element={
          <>
            <PageTitle breadcrumbs={accountBreadCrumbs}>Settings</PageTitle>
            <Settings profile={profile} />
          </>
        }
      />
      {/* <Route index element={<Navigate to='/crafted/account/overview' />} /> */}
    </Routes>
  )
}

// export default
const mapStateToProps = createStructuredSelector({
  profile: selectUserProfileInfo,
})

export default connect(mapStateToProps)(AccountPage)
