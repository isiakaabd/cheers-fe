// import {SignInMethod} from "./cards/SignInMethod"
// import {ConnectedAccounts} from "./cards/ConnectedAccounts"
// import {EmailPreferences} from "./cards/EmailPreferences"
// import {Notifications} from "./cards/Notifications"
// import {DeactivateAccount} from "./cards/DeactivateAccount"

import ProfileDetails from "./cards/ProfileDetails"
import {FC} from "react"
type Profile = {
  profile: any
}
// const AccountHeader: FC<AccountProfileProps> = ({actionFetchUserProfile, profile}) => {
const Settings: FC<Profile> = ({profile}) => {
  return (
    <>
      <ProfileDetails profile={profile} />
      {/* <SignInMethod />
      <ConnectedAccounts />
      <EmailPreferences />
      <Notifications />
      <DeactivateAccount /> */}
    </>
  )
}
export {Settings}
