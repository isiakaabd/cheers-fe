/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useEffect} from "react"
import {connect} from "react-redux"
import {useParams} from "react-router"
import {Link} from "react-router-dom"
import {createStructuredSelector} from "reselect"

import {
  actionDeleteInventory,
  actionFetchInventory,
  actionFetchInventoryReview,
} from "../../../redux/inventory/inventory.actions"
import {selectAnInventory, selectInventoryReview} from "../../../redux/inventory/inventory.selector"
import {KTSVG, toAbsoluteUrl} from "../../../_metronic/helpers"
import {StatisticsWidget1} from "../../../_metronic/partials/widgets"

type Props = {
  fetchInventory: (args: number) => any
  deleteInventory: (args: number) => any
  inventory: any
  review: any
  inventoryReview: (args: number) => any
}

const ShowInventories: React.FC<Props> = ({
  fetchInventory,
  deleteInventory,
  inventory,
  review,
  inventoryReview,
}) => {
  const {inventoryId} = useParams()

  const handleRatingDisplay = (average_rating: number) => {
    const rating = []
    for (let i = 0; i < average_rating; i++) {
      rating.push(
        <div className='rating-label me-2 checked'>
          <i className='bi bi-star-fill fs-5'></i>
        </div>
      )
    }

    return rating
  }

  useEffect(() => {
    if (inventoryId) {
      inventoryReview(parseInt(inventoryId))
      fetchInventory(parseInt(inventoryId))
    }
    //eslint-disable-next-line
  }, [inventoryId])
  return (
    <div className={`card`}>
      <div className='d-flex justify-content-between'>
        <StatisticsWidget1
          className='card-xl-stretch mb-xl-8'
          image='abstract-4.svg'
          title={inventory?.title}
          time={inventory?.category?.title}
          description={inventory.description}
        />

        <div className='m-5'>
          <button
            className='btn btn-bg-danger btn-color-muted btn-active-color-white btn-sm px-4 me-2'
            data-kt-menu-trigger='click'
            data-kt-menu-placement='bottom-end'
            data-kt-menu-flip='top-end'
          >
            Delete
          </button>

          <div
            className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold w-200px'
            data-kt-menu='true'
          >
            {/* begin::Menu item */}
            <div className='menu-item px-3'>
              <div className='menu-content fs-6 text-dark fw-bolder px-3 py-4'>Are you sure</div>
            </div>
            {/* end::Menu item */}
            {/* begin::Menu separator */}
            <div className='separator mb-3 opacity-75'></div>
            {/* end::Menu separator */}
            {/* begin::Menu item */}
            <div className='menu-item px-3'>
              <a
                href='#'
                className='menu-link px-3'
                onClick={() => inventoryId && deleteInventory(parseInt(inventoryId))}
              >
                Yes
              </a>
            </div>
            {/* end::Menu item */}
            {/* begin::Menu item */}
            <div className='menu-item px-3'>
              <a href='#' className='menu-link px-3'>
                No
              </a>
            </div>
          </div>
          <Link
            to={`/inventory/${inventory?.id}/edit`}
            className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4'
          >
            Edit
          </Link>
        </div>
      </div>

      <div className=''>
        <div className='card-header border-0'>
          <h3 className='card-title fw-bolder text-dark'>Images</h3>
        </div>
        <div className='d-flex card-body flex-wrap'>
          {inventory?.media?.map((media: any) => (
            <div className='col-4'>
              <img
                src={media?.original_url}
                className='img-thumbnail h-75 w-75'
                alt={media?.file_name}
              ></img>
            </div>
          ))}
        </div>
      </div>
      {/* begin::Header */}
      <div className='card-header border-0'>
        <h3 className='card-title fw-bolder text-dark'>Reviews and comments</h3>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className='card-body pt-2'>
        {/* begin::Item */}
        {review?.links?.map((item: any) => (
          <>
            <div className='d-flex justify-content-between'>
              <div className='d-flex align-items-center mb-7'>
                {/* begin::Avatar */}
                <div className='symbol symbol-50px me-5'>
                  <img src={item?.user?.avatar} className='' alt='' />
                </div>
                {/* end::Avatar */}
                {/* begin::Text */}
                <div className='flex-grow-1'>
                  <a
                    href={item?.url}
                    className='text-dark fw-bolder cursor-pointer text-hover-primary fs-6'
                  >
                    {item?.label}
                  </a>
                  <span className='text-muted cursor-pointer d-block fw-bold'>
                    {item?.active ? "Active" : "Inactive"}
                  </span>
                </div>
                {/* end::Text */}
              </div>

              <div className='d-flex'>{handleRatingDisplay(item.rating) || "not rated"}</div>
            </div>
          </>
        ))}
        {/* end::Item */}
      </div>
      {/* end::Body */}
    </div>
  )
}

const mapStateToProps = createStructuredSelector({
  inventory: selectAnInventory,
  review: selectInventoryReview,
})

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchInventory: (inventoryId: number) => dispatch(actionFetchInventory(inventoryId)),
    deleteInventory: (inventoryId: number) => dispatch(actionDeleteInventory(inventoryId)),
    inventoryReview: (inventoryId: number) => dispatch(actionFetchInventoryReview(inventoryId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowInventories)

// const mapStateToProps = createStructuredSelector({
//   inventories: selectAllInventories,
// })

// const mapDispatchToProps = (dispatch: any) => {
//   return {
//     actionFetchAllMyInventory: () => dispatch(actionFetchAllMyInventory()),
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Inventories)
