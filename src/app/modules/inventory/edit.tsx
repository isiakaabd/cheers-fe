import {Form, Formik} from "formik"
import * as Yup from "yup"

import {FC, useEffect} from "react"
import {PageTitle} from "../../../_metronic/layout/core"
import clsx from "clsx"
import {createStructuredSelector} from "reselect"

import {
  selectAllCategories,
  selectAnInventory,
  selectInventoryLoading,
} from "../../../redux/inventory/inventory.selector"
import {
  // actionCreateInventory,
  actionFetchCategory,
  actionFetchInventory,
  actionUpdateInventory,
} from "../../../redux/inventory/inventory.actions"
import {connect} from "react-redux"
import {useParams} from "react-router"

type EditInventoryType = {
  updateInventory: (inventory: {}, args: number) => any
  loading: boolean
  categories: any
  inventory: any
  fetchCategories: () => any
  fetchInventory: (args: number) => any
}
const EditInventories: FC<EditInventoryType> = ({
  fetchCategories,
  loading,
  categories,
  inventory,
  fetchInventory,
  updateInventory,
}) => {
  useEffect(() => {
    fetchCategories()
  }, [fetchCategories])

  const {inventoryId} = useParams()

  useEffect(() => {
    inventoryId && fetchInventory(parseInt(inventoryId))
  }, [fetchInventory, inventoryId])

  const handleCreateInventory = (values: any) => {
    const formData = new FormData()

    formData.append("_method", "put")
    formData.append("title", values.title)
    formData.append("category_id", values.category)
    formData.append("price", values.price)
    formData.append("description", values.description)

    for (let i = 0; i < values.gallery.length; i++) {
      formData.append("gallery[]", values.gallery[i])
    }

    inventoryId && updateInventory(formData, parseInt(inventoryId))
  }
  return (
    <>
      <PageTitle breadcrumbs={[]}>Update Inventories</PageTitle>
      <Formik
        initialValues={{
          title: (inventory.title as string) || "",
          category: (inventory.category_id as number) || "",
          price: (inventory.price as number) || "",
          description: (inventory.description as string) || "",
          gallery: [],
        }}
        enableReinitialize
        validationSchema={Yup.object().shape({
          title: Yup.string().required(),
          category: Yup.number().required(),
          price: Yup.number().required(),
          description: Yup.string().required(),
        })}
        onSubmit={(values, {setSubmitting}) => {
          handleCreateInventory(values)

          setSubmitting(false)
        }}
      >
        {({setFieldValue, values, errors, touched, getFieldProps, isSubmitting, isValid}) => (
          <Form id='kt_login_signin_form'>
            {/* <div className='form-group'>
              <label>Title</label>
              <input type='title' className='form-control' placeholder='Enter title' />
              <small id='emailHelp' className='form-text text-muted'>
                We'll never share your email with anyone else.
              </small>
            </div> */}

            <div className='fv-row mb-10'>
              <label className='form-label fs-6 fw-bolder text-dark'>Title</label>
              <input
                placeholder='Title'
                {...getFieldProps("title")}
                className={clsx(
                  "form-control form-control-lg",
                  {"is-invalid": touched.title && errors.title},
                  {
                    "is-valid": touched.title && !errors.title,
                  }
                )}
                type='text'
                name='title'
                autoComplete='off'
              />
              {touched.title && errors.title && (
                <div className='fv-plugins-message-container'>
                  <span role='alert'>{errors.title}</span>
                </div>
              )}
            </div>

            <div className='fv-row mb-10'>
              <label className='form-label fs-6 fw-bolder text-dark'>Category</label>
              <select
                placeholder='Category'
                {...getFieldProps("category")}
                className={clsx(
                  "form-control form-control-lg",
                  {"is-invalid": touched.category && errors.category},
                  {
                    "is-valid": touched.category && !errors.category,
                  }
                )}
                name='category'
                autoComplete='off'
              >
                <option selected>select category</option>
                {categories?.map((category: any) => (
                  <>
                    <option value={category?.id}>{category?.title}</option>
                  </>
                ))}
              </select>
              {touched.category && errors.category && (
                <div className='fv-plugins-message-container'>
                  <span role='alert'>{errors.category}</span>
                </div>
              )}
            </div>

            <div className='fv-row mb-10'>
              <label className='form-label fs-6 fw-bolder text-dark'>Price</label>
              <input
                placeholder='Price'
                {...getFieldProps("price")}
                className={clsx(
                  "form-control form-control-lg",
                  {"is-invalid": touched.price && errors.price},
                  {
                    "is-valid": touched.price && !errors.price,
                  }
                )}
                type='number'
                name='price'
                autoComplete='off'
              />
              {touched.price && errors.price && (
                <div className='fv-plugins-message-container'>
                  <span role='alert'>{errors.price}</span>
                </div>
              )}
            </div>

            <div className='fv-row mb-10'>
              <label className='form-label fs-6 fw-bolder text-dark'>Description</label>
              <textarea
                placeholder='Description'
                {...getFieldProps("description")}
                className={clsx(
                  "form-control form-control-lg",
                  {"is-invalid": touched.description && errors.description},
                  {
                    "is-valid": touched.description && !errors.description,
                  }
                )}
                name='description'
                autoComplete='off'
              ></textarea>
              {touched.description && errors.description && (
                <div className='fv-plugins-message-container'>
                  <span role='alert'>{errors.description}</span>
                </div>
              )}
            </div>

            <div className='fv-row mb-10'>
              <label className='form-label fs-6 fw-bolder text-dark'>Images</label>
              <input
                // {...getFieldProps('gallery')}
                className={clsx(
                  "form-control form-control-lg",
                  {"is-invalid": touched.gallery && errors.gallery},
                  {
                    "is-valid": touched.gallery && !errors.gallery,
                  }
                )}
                type='file'
                name='gallery'
                multiple
                accept='image/*'
                onChange={(event) => {
                  setFieldValue("gallery", event.currentTarget.files)
                }}
              />
              {touched.gallery && errors.gallery && (
                <div className='fv-plugins-message-container'>
                  <span role='alert'>{errors.gallery}</span>
                </div>
              )}
            </div>

            <div className='text-center'>
              <button
                type='submit'
                id='kt_sign_in_submit'
                className='btn btn-lg btn-primary w-100 mb-20'
                disabled={isSubmitting}
              >
                {!loading && <span className='indicator-label'>Continue</span>}
                {loading && (
                  <span className='indicator-progress' style={{display: "block"}}>
                    Please wait...
                    <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                  </span>
                )}
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  )
}

const mapStateToProps = createStructuredSelector({
  categories: selectAllCategories,
  loading: selectInventoryLoading,
  inventory: selectAnInventory,
})

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateInventory: (inventory: object, inventoryId: number) =>
      dispatch(actionUpdateInventory(inventory, inventoryId)),
    fetchInventory: (inventoryId?: number) => dispatch(actionFetchInventory(inventoryId)),
    fetchCategories: () => dispatch(actionFetchCategory()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditInventories)
