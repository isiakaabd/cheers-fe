import {FC, useEffect} from "react"
import {Link, Navigate, useNavigate} from "react-router-dom"
import {connect} from "react-redux"
import {createStructuredSelector} from "reselect"
import {actionFetchAllMyInventory} from "../../../redux/inventory/inventory.actions"
import {selectAllInventories} from "../../../redux/inventory/inventory.selector"
import {KTSVG, toAbsoluteUrl} from "../../../_metronic/helpers"
import {PageTitle} from "../../../_metronic/layout/core"
import InventoryPagination from "./InventoryPagination"

type InventoriesProps = {
  actionFetchAllMyInventory: () => any
  inventories: any
}

const Inventories: FC<InventoriesProps> = ({actionFetchAllMyInventory, inventories}) => {
  useEffect(() => {
    actionFetchAllMyInventory()
  }, [actionFetchAllMyInventory])

  const handleRatingDisplay = (average_rating: number) => {
    const rating = []
    for (let i = 0; i < average_rating; i++) {
      rating.push(
        <div className='rating-label me-2 checked'>
          <i className='bi bi-star-fill fs-5'></i>
        </div>
      )
    }

    return rating
  }

  // type item = {
  //   collection_name: string
  //   conversions_disk: string
  //   created_at: string
  //   custom_properties: any[]
  //   disk: string
  //   file_name: string
  //   generated_conversions: any[]
  //   id: number
  //   manipulations: any[]
  //   mime_type: string
  //   model_id: number
  //   model_type: string
  //   name: string
  //   order_column: number
  //   original_url: string
  //   preview_url: any
  //   responsive_images: any[]
  //   size: string
  //   updated_at: string
  //   uuid: string
  // }

  const navigate = useNavigate()
  return (
    <>
      <PageTitle breadcrumbs={[]}>Inventories</PageTitle>
      <div className={`card`}>
        {/* begin::Header */}
        <div className='card-header border-0 pt-5'>
          <h3 className='card-title align-items-start flex-column'>
            <span className='card-label fw-bolder fs-3 mb-1'>All Inventories</span>
            <span className='text-muted mt-1 fw-bold fs-7'>
              {inventories?.total || 0} available inventories
            </span>
          </h3>
          <div className='card-toolbar'>
            {/* begin::Menu */}
            <div className='menu-item px-3'>
              <div className='menu-content px-3 py-3'>
                <Link className='btn btn-primary btn-sm px-4' to='/inventory/create'>
                  Create Inventory
                </Link>
              </div>
            </div>

            {/* end::Menu 2 */}
            {/* end::Menu */}
          </div>
        </div>
        {/* end::Header */}
        {/* begin::Body */}
        <div className='card-body py-3'>
          {/* begin::Table container */}
          <div className='table-responsive'>
            {/* begin::Table */}
            <table className='table align-middle gs-0 gy-4'>
              {/* begin::Table head */}
              <thead>
                <tr className='fw-bolder text-muted bg-light'>
                  <th className='ps-4 min-w-300px rounded-start'>Name</th>
                  <th className='min-w-125px'>Price</th>
                  <th className='min-w-125px'>Description</th>
                  <th className='min-w-200px'>Category</th>
                  {/* <th className='min-w-150px'>Rating</th> */}
                  {/* <th className='min-w-200px text-end rounded-end'></th> */}
                </tr>
              </thead>
              {/* end::Table head */}
              {/* begin::Table body */}
              <tbody>
                {inventories?.data?.map((inventory: any, index: number) => (
                  // <Link
                  //   to={`/inventory/${inventory?.id}/edit`}
                  //   className='w-full'
                  //   style={{width: "100%", background: "red important"}}
                  // >
                  <tr
                    className='tr-over cursor-pointer'
                    key={index}
                    onClick={() => navigate(`/inventory/${inventory?.id}`)}
                  >
                    <td>
                      <div className='d-flex align-items-center'>
                        <div className='symbol symbol-50px me-5'>
                          {/* {inventory?.media?.map((item: item) => (
                            <span className='symbol-label bg-light'>
                              <img
                                src={item?.original_url}
                                key={item.uuid}
                                className='h-75 align-self-end'
                                alt={item.file_name}
                              />
                            </span>
                          ))} */}
                        </div>
                        <div className='d-flex justify-content-start flex-column'>
                          <span className='text-dark fw-bolder  mb-1 fs-6' title={inventory?.title}>
                            {inventory?.title}
                          </span>
                          <span
                            className='text-muted fw-bold text-muted d-block fs-7'
                            title={inventory?.category?.title}
                          >
                            {inventory?.category?.title}
                          </span>
                        </div>
                      </div>
                    </td>
                    <td title={inventory?.price}>
                      <span className='text-dark fw-bolder  d-block mb-1 fs-6'>
                        #{inventory?.price}
                      </span>
                    </td>

                    <td title={inventory?.description}>
                      <span className='text-dark fw-bolder  d-block mb-1 fs-6'>
                        {inventory?.description.substring(0, 20)}{" "}
                        {inventory?.description?.length > 20 ? "..." : ""}
                      </span>
                      {/* <span className='text-muted fw-bold text-muted d-block fs-7'>Paid</span> */}
                    </td>

                    <td title={inventory?.category?.title}>
                      <span className='text-dark fw-bolder  d-block mb-1 fs-6'>
                        {inventory?.category?.title}
                      </span>
                    </td>

                    {/* <td>
                      <div className='d-flex'>
                        {handleRatingDisplay(inventory?.average_rating) || "not rated"}
                      </div>
                    </td> */}
                    {/* 
                    <td className='text-end'>
                      <Link
                        to={`/inventory/${inventory?.id}`}
                        className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'
                      >
                        View
                      </Link>
                      <Link
                        to={`/inventory/${inventory?.id}/edit`}
                        className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4'
                      >
                        Edit
                      </Link>
                    </td> */}
                    {/* </Link> */}
                  </tr>
                  // </Link>
                ))}
              </tbody>
              {/* end::Table body */}
            </table>
            {/* end::Table */}
          </div>
          {/* end::Table container */}
        </div>
        {/* begin::Body */}
      </div>
      <InventoryPagination />
    </>
  )
}

const mapStateToProps = createStructuredSelector({
  inventories: selectAllInventories,
})

const mapDispatchToProps = (dispatch: any) => {
  return {
    actionFetchAllMyInventory: () => dispatch(actionFetchAllMyInventory()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Inventories)
