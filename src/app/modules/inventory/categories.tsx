import {FC, useEffect} from "react"
import {connect} from "react-redux"
import {createStructuredSelector} from "reselect"
import {actionFetchCategory} from "../../../redux/inventory/inventory.actions"
import {selectAllCategories} from "../../../redux/inventory/inventory.selector"
import {PageTitle} from "../../../_metronic/layout/core"
import {StatisticsWidget1} from "../../../_metronic/partials/widgets"
type Prop = {
  fetchCategories: () => any
  categories: any
}
const Categories: FC<Prop> = ({fetchCategories, categories}) => {
  useEffect(() => {
    fetchCategories()
    //eslint-disable-next-line
  }, [])

  type category = {
    title: string
    created_at: any
    deleted_at: any
    description: string
    id: number
  }
  return (
    <>
      <PageTitle breadcrumbs={[]}>Categories</PageTitle>
      <div className='row g-5 g-xl-8'>
        {categories?.map((category: category) => {
          const {id, title, description, created_at} = category
          return (
            <div className='col-xl-4' key={id}>
              <StatisticsWidget1
                className='card-xl-stretch mb-xl-8'
                image='abstract-4.svg'
                title={title}
                time={created_at}
                description={description}
              />
            </div>
          )
        })}
      </div>
    </>
  )
}

const mapStateToProps = createStructuredSelector({
  categories: selectAllCategories,
})

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchCategories: () => dispatch(actionFetchCategory()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories)
