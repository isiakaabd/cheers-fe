export const ALERT_TYPES = {
  SUCCESS: 'SUCCESS',
  WARN: 'WARN',
  ERROR: 'ERROR',
  CLEAR: 'CLEAR'
};