import {createSelector} from "reselect"

const selectInventory = (state: any) => state.inventoryReducer

export const selectAllInventories = createSelector(
  [selectInventory],
  (inventory) => inventory.inventories
)
export const selectAnInventory = createSelector(
  [selectInventory],
  (inventory) => inventory.inventory
)
export const selectAllCategories = createSelector(
  [selectInventory],
  (inventory) => inventory.categories
)
export const selectInventoryLoading = createSelector(
  [selectInventory],
  (inventory) => inventory.loading
)
export const selectInventoryReview = createSelector(
  [selectInventory],
  (inventory) => inventory.review
)
