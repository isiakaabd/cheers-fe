import {requestFetchInventoryReviews} from "./../../requests/inventory.requests"
import {
  requestCreateInventory,
  requestDeleteInventory,
  requestFetchAllCategories,
  requestFetchAllMyInventories,
  requestFetchInventory,
  requestUpdateInventory,
} from "../../requests/inventory.requests"
import {alertActions} from "../alert/alert.actions"
import {InventoryTypes} from "./inventory.types"

export const actionFetchAllMyInventory = () => {
  return async (dispatch: any, getState: () => any) => {
    try {
      dispatch({
        type: InventoryTypes.FETCH_ALL_INVENTORY_PROCESS,
      })

      const {
        data: {data},
      } = await requestFetchAllMyInventories()

      dispatch({
        type: InventoryTypes.FETCH_ALL_INVENTORY_SUCCESS,
        payload: data,
      })
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const actionCreateInventory = (inventory: {}) => {
  return async (dispatch: any) => {
    try {
      const {
        data: {message},
      } = await requestCreateInventory(inventory)

      dispatch(alertActions.success(message))

      setTimeout(() => (window.location.href = "/inventory"), 3000)
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const actionFetchCategory = () => {
  return async (dispatch: any) => {
    dispatch({
      type: InventoryTypes.FETCH_ALL_CATEGORY_PROCESS,
    })
    try {
      const {
        data: {data},
      } = await requestFetchAllCategories()

      dispatch({
        type: InventoryTypes.FETCH_ALL_CATEGORY_SUCCESS,
        payload: data,
      })
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const actionUpdateInventory = (inventory: {}, inventoryId: number) => {
  return async (dispatch: any) => {
    try {
      const {
        data: {message},
      } = await requestUpdateInventory(inventory, inventoryId)

      dispatch(alertActions.success(message))

      setTimeout(() => (window.location.href = "/inventory"), 3000)
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const actionDeleteInventory = (inventoryId: number) => {
  return async (dispatch: any) => {
    try {
      await requestDeleteInventory(inventoryId)

      dispatch(alertActions.success("Inventory deleted successfully"))

      setTimeout(() => (window.location.href = "/inventory"), 3000)
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const actionFetchInventory = (inventoryId?: number) => {
  return async (dispatch: any, getState: () => any) => {
    try {
      dispatch({
        type: InventoryTypes.FETCH_INVENTORY_PROCESS,
      })

      const {
        data: {data},
      } = await requestFetchInventory(inventoryId)

      dispatch({
        type: InventoryTypes.FETCH_INVENTORY_SUCCESS,
        payload: data,
      })
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}
export const actionFetchInventoryReview = (inventoryId?: number) => {
  return async (dispatch: any, getState: () => any) => {
    try {
      dispatch({
        type: InventoryTypes.FETCH_REVIEW_CATEGORY_PROCESS,
      })

      const {
        data: {data},
      } = await requestFetchInventoryReviews(inventoryId)

      dispatch({
        type: InventoryTypes.FETCH_REVIEW_CATEGORY_SUCCESS,
        payload: data,
      })
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}
