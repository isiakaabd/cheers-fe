import {InventoryAction, InventoryReducer, InventoryTypes} from "./inventory.types"

const INITIAL_STATE: InventoryReducer = {
  inventories: [],
  inventory: {},
  categories: [],
  review: {},
  loading: false,
}
// eslint-disable-next-line import/no-anonymous-default-export
export default (state: InventoryReducer = INITIAL_STATE, {type, payload}: InventoryAction) => {
  switch (type) {
    case InventoryTypes.FETCH_ALL_INVENTORY_PROCESS:
      return {...state, loading: true, inventories: []}
    case InventoryTypes.FETCH_ALL_INVENTORY_SUCCESS:
      return {...state, loading: false, inventories: payload}

    case InventoryTypes.FETCH_INVENTORY_PROCESS:
      return {...state, loading: true, inventory: {}}
    case InventoryTypes.FETCH_INVENTORY_SUCCESS:
      return {...state, loading: false, inventory: payload}

    case InventoryTypes.FETCH_ALL_CATEGORY_PROCESS:
      return {...state, loading: true, categories: []}
    case InventoryTypes.FETCH_ALL_CATEGORY_SUCCESS:
      return {...state, loading: false, categories: payload}
    case InventoryTypes.FETCH_REVIEW_CATEGORY_PROCESS:
      return {...state, loading: true, categories: []}
    case InventoryTypes.FETCH_REVIEW_CATEGORY_SUCCESS:
      return {...state, loading: false, review: payload}
    default:
      return state
  }
}
