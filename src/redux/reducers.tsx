import { combineReducers } from "redux";
import authReducer from "./auth/auth.reducer";
import alertReducer from "./alert/alert.reducers";
import inventoryReducer from './inventory/inventory.reducer';

const rootReducer = combineReducers({
    authReducer,
    alertReducer,
    inventoryReducer
})

export default rootReducer;