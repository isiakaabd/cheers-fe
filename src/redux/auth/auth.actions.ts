import {useNavigate} from "react-router-dom"
import {
  requestForgotPassword,
  requestResendVerificationEmail,
  requestResetPassword,
  requestSignIn,
  requestSignUp,
  requestVendorProfile,
  requestVerifyEmail,
} from "../../requests/auth.requests"
import {setAccessToken} from "../../requests/helpers"
import {alertActions} from "../alert/alert.actions"
import {
  AuthTypes,
  ResetPasswordTypes,
  SignInTypes,
  SignUpDetialsTypes,
  VerifyEmailTypes,
} from "./auth.types"

export const actionSignUpUser = (signupDetials: SignUpDetialsTypes) => {
  return async (dispatch: any) => {
    dispatch({
      type: AuthTypes.VENDOR_SIGN_UP_PROCESS,
    })

    try {
      const {
        data: {message},
      } = await requestSignUp(signupDetials)

      dispatch({
        type: AuthTypes.VENDOR_SIGN_IN_SUCCESS,
        payload: message,
      })

      dispatch(alertActions.success(message))

      setTimeout(() => (window.location.href = "/auth/verify?email=" + signupDetials.email), 3000)
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))

      const {email, username, first_name, last_name, phone, password} = error.response.data.errors

      email && dispatch(alertActions.error(email[0]))

      username && dispatch(alertActions.error(username[0]))

      first_name && dispatch(alertActions.error(first_name[0]))

      last_name && dispatch(alertActions.error(last_name[0]))

      phone && dispatch(alertActions.error(phone[0]))

      password && dispatch(alertActions.error(password[0]))
    }
  }
}

export const verifyEmail = (data: VerifyEmailTypes) => {
  return async (dispatch: any) => {
    try {
      const {
        data: {message},
      } = await requestVerifyEmail(data)

      dispatch(alertActions.success(message))

      setTimeout(() => (window.location.href = "/auth"), 3000)

      return message
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const resendVerificationEmail = (email: string) => {
  return async (dispatch: any) => {
    try {
      const {
        data: {message},
      } = await requestResendVerificationEmail(email)

      dispatch(alertActions.success(message))

      return message
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const actionSignInUser = (credentials: SignInTypes) => {
  return async (dispatch: any) => {
    try {
      const {
        data: {
          message,
          data: {token},
        },
      } = await requestSignIn(credentials)

      dispatch(alertActions.success(message))

      setAccessToken(token)

      setTimeout(() => (window.location.href = "/dashboard"), 2000)
      return message
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))

      const {email, password} = error.response.data.errors

      email && dispatch(alertActions.error(email[0]))

      password && dispatch(alertActions.error(password[0]))
    }
  }
}

export const actionForgotPassword = (email: string) => {
  return async (dispatch: any) => {
    try {
      const {
        data: {message},
      } = await requestForgotPassword(email)

      dispatch(alertActions.success(message))

      setTimeout(() => (window.location.href = "/auth/reset-password?email=" + email), 3000)

      return message
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}

export const actionResetassword = (values: ResetPasswordTypes) => {
  return async (dispatch: any) => {
    try {
      const {
        data: {message},
      } = await requestResetPassword(values)

      dispatch(alertActions.success(message))

      setTimeout(() => (window.location.href = "/auth/login"), 3000)
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))

      const {email, password, password_confirmation, otp} = error.response.data.errors

      email && dispatch(alertActions.error(email[0]))

      password && dispatch(alertActions.error(password[0]))

      otp && dispatch(alertActions.error(otp[0]))

      password_confirmation && dispatch(alertActions.error(password_confirmation[0]))
    }
  }
}

export const actionFetchUserProfile = () => {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: AuthTypes.VENDOR_PROFILE_PROCESS,
      })

      const {
        data: {
          data: {vendor},
        },
      } = await requestVendorProfile()

      dispatch({
        type: AuthTypes.VENDOR_PROFILE_SUCCESS,
        payload: vendor,
      })
    } catch (error: any) {
      dispatch(alertActions.error(error.response.data.message))
    }
  }
}
