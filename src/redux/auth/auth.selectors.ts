import { createSelector } from "reselect";

const selectAuth = (state: any) => state.authReducer;

export const selectUserProfileInfo = createSelector([selectAuth], auth => auth.auth_user);

