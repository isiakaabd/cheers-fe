export const AuthTypes = {
  VENDOR_SIGN_UP_PROCESS: "VENDOR_SIGN_UP_PROCESS",
  VENDOR_SIGN_UP_SUCCESS: "VENDOR_SIGN_UP_SUCCESS",

  VENDOR_SIGN_IN_PROCESS: "VENDOR_SIGN_IN_PROCESS",
  VENDOR_SIGN_IN_SUCCESS: "VENDOR_SIGN_IN_SUCCESS",

  VENDOR_PROFILE_PROCESS: "VENDOR_PROFILE_PROCESS",
  VENDOR_PROFILE_SUCCESS: "VENDOR_PROFILE_SUCCESS",
}

export interface AuthReducer {
  loading: boolean
  token: String
  auth_user: object
}

export interface AuthAction {
  type: string
  payload: any
}

export interface SignUpDetialsTypes {
  first_name: string
  last_name: string
  email: string
  phone?: string
  password: string
  password_confirmation: string
  username: string
  profile_picture?: string
}

export interface VerifyEmailTypes {
  email: string
  token: number | string
}

export interface SignInTypes {
  email: string
  password: string
}

export interface ResetPasswordTypes {
  email: string
  password: string
  password_confirmation: string
  otp: string
}
export interface userProfile {
  loading: boolean
  profile: object
}
