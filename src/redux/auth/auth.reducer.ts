import { AuthAction, AuthReducer, AuthTypes } from "./auth.types"

const INITIAL_STATE: AuthReducer = {
    loading: false,
    token: '',
    auth_user: {}
}

export default (state: AuthReducer = INITIAL_STATE, { type, payload }: AuthAction) => {
    switch (type) {
        case AuthTypes.VENDOR_SIGN_UP_PROCESS:
            return { ...state, loading: true, token: '' }
        case AuthTypes.VENDOR_SIGN_UP_SUCCESS:
            return { ...state, loading: false, token: payload }

        case AuthTypes.VENDOR_PROFILE_PROCESS:
            return { ...state, loading: true, auth_user: {} }
        case AuthTypes.VENDOR_PROFILE_SUCCESS:
            return { ...state, loading: false, auth_user: payload }

        default:
            return state;
    }
};