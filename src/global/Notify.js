import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ALERT_TYPES } from '../redux/alert/alert.types';
import { alertActions } from '../redux/alert/alert.actions';
import { toast } from 'react-toastify';

const colorTypes = {
  SUCCESS: 'success',
  WARN: 'warning',
  ERROR: 'danger'
};

export default function Notify({ children }) {
  const dispatch = useDispatch();
  const selector = useSelector(state => state.alertReducer);
  const [alert, setAlert] = React.useState({
    message: null,
    icon: null,
    color: ''
  });

  useEffect(() => {
    toast.error(alert.message)
  }, [alert])
  const timeout = 5000; // 5 seconds

  // Message will be cleared after 5 seconds
  useEffect(() => {
    switch (selector.type) {
      case ALERT_TYPES.SUCCESS:
        toast.success(selector.message);

        // setTimeout(() => {
        //   setAlert({
        //     message: null,
        //     icon: null,
        //     color: ''
        //   });
        //   dispatch(alertActions.clear());
        // }, timeout);
        break;
      case ALERT_TYPES.WARN:
        toast.warn(selector.message);
        // setAlert({
        //   message: selector.message,
        //   icon: '/media/icons/alert-warning.svg',
        //   color: colorTypes.WARN
        // });
        // setTimeout(() => {
        //   setAlert({
        //     message: null,
        //     icon: null,
        //     color: ''
        //   });
        //   dispatch(alertActions.clear());
        // }
        //   , timeout);
        break;
      case ALERT_TYPES.ERROR:
        toast.error(selector.message);
        // setAlert({
        //   message: selector.message,
        //   icon: '/media/icons/alert-error.svg',
        //   color: colorTypes.ERROR
        // });
        // setTimeout(() => {
        //   setAlert({
        //     message: null,
        //     icon: null,
        //     color: ''
        //   });
        //   dispatch(alertActions.clear());
        // }
        //   , timeout);
        break;
      case ALERT_TYPES.CLEAR:
        setAlert({
          message: null,
          icon: null,
          color: ''
        });
        break;
      default:
        break;
    }
  }, [selector]);

  return (
    <div>
      {/* <div className={`alert-container ${alert.message ? 'open' : ''}`}> */}
      {alert.message && (
        <>
          {/* <div className={'alert alert-'+alert.color}><img src={alert.icon} alt="alert" /></div>
            <div className='_text'>{alert.message}</div>
            <div className='_close' onClick={() => dispatch(alertActions.clear())}>
              <img src="/media/icons/close.svg" alt="close" />
            </div> */}

          {/* <div class={`alert alert-custom alert-outline-${alert.color} fade show mb-5`} role="alert">
              <div class="alert-icon"><i class="flaticon-warning"></i></div>
              <div class="alert-text">{alert.message}</div>
              <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"><i class="ki ki-close"></i></span>
                </button>
              </div>
            </div> */}

          {/* {toast.error('aye aye sir')} */}
        </>
      )}
      {/* </div> */}
      {children}
    </div>
  );
}
