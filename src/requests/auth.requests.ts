import Cheers from "./CheersApi"

export const requestSignUp = async (signUpDetials: {}) => {
  return await Cheers.post(`/vendors/sign-up`, signUpDetials)
}

export const requestVendorProfile = async () => {
  return await Cheers.get(`/vendors`)
}

export const requestSignIn = async (credentials: {}) => {
  return await Cheers.post(`/vendors/sign-in`, credentials)
}

export const requestVerifyEmail = async (data: object) => {
  return await Cheers.put(`/vendors/verify`, data)
}

export const requestResendVerificationEmail = async (email: string) => {
  return await Cheers.post(`/vendors/verify/resend-token`, {email})
}

export const requestForgotPassword = async (email: string) => {
  return await Cheers.post(`/vendors/forgot-password`, {email})
}

export const requestResetPassword = async (params: object) => {
  return await Cheers.post(`/vendors/reset-password`, params)
}
