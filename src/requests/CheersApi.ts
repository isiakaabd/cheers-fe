import axios, {AxiosRequestConfig, AxiosInstance, AxiosResponse} from "axios"
// import { store } from '../redux/store';
import {getToken} from "./helpers"
const Cheers: AxiosInstance = axios.create()

/**
 * this interceptor handles all network request
 * made to the Backend service
 */
Cheers.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    config.baseURL = "https://api.cheers.global/api/"
    //  (process.env.NODE_ENV as string) == 'development' ? 'http://127.0.0.1:8000/api/' :
    config.headers["Authorization"] = getToken() && `Bearer ${getToken()}`
    config.headers["Content-Type"] = "application/json"
    config.headers["Accept"] = "application/json"
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

/**
 * this interceptor handle all network response from the
 * Backend service
 */
Cheers.interceptors.response.use(
  (response: AxiosResponse) => {
    return response
  },
  (error) => {
    console.error("error_status", error.response.status)
    return Promise.reject(error)
  }
)
export default Cheers
