import Cheers from "./CheersApi"

export const requestFetchAllMyInventories = async () => {
  return await Cheers.get("/vendors/inventories")
}

export const requestFetchInventory = async (inventoryId?: number) => {
  return await Cheers.get(`/vendors/inventories/${inventoryId}`)
}
export const requestFetchInventoryReviews = async (inventoryId?: number) => {
  return await Cheers.get(`/vendors/inventories/${inventoryId}/reviews`)
}

export const requestCreateInventory = async (inventory: {}) => {
  return await Cheers.post("/vendors/inventories", inventory)
}

export const requestUpdateInventory = async (inventory: {}, inventoryId: number) => {
  return await Cheers.post(`/vendors/inventories/${inventoryId}`, inventory)
}

export const requestDeleteInventory = async (inventoryId: number) => {
  return await Cheers.delete(`/vendors/inventories/${inventoryId}`)
}

export const requestFetchAllCategories = async () => {
  return await Cheers.get("/vendors/categories")
}
